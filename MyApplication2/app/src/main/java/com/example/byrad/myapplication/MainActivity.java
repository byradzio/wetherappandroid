package com.example.byrad.myapplication;

import android.os.Bundle;
import android.util.Log;
import android.support.v7.app.AppCompatActivity;
import android.graphics.Typeface;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import im.delight.android.location.SimpleLocation;

public class MainActivity extends AppCompatActivity {

    TextView cityField, detailsField, currentTemperatureField, humidity_field, pressure_field, weatherIcon, updatedField;

    Typeface weatherFont;
    private SimpleLocation location;
    double lon;
    double lat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        GetLocation();
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        GetWether();



    }
    @Override
    protected void onResume(){
        super.onResume();
        location.beginUpdates();
    }
    @Override
    protected void onPause() {
        location.endUpdates();
        super.onPause();
    }
    public void screenTapped(View view){
        GetLocation();
        GetWether();
    }

    private void GetLocation(){
        location = new SimpleLocation(this);
        if (!location.hasLocationEnabled()) {
            SimpleLocation.openSettings(this);
        }
        lon = location.getLongitude();
        lat = location.getLatitude();
    }
    private void GetWether(){
        weatherFont = Typeface.createFromAsset(getAssets(), "fonts/weathericons-regular-webfont.ttf");

        cityField = (TextView)findViewById(R.id.city_field);
        updatedField = (TextView)findViewById(R.id.updated_field);
        detailsField = (TextView)findViewById(R.id.details_field);
        currentTemperatureField = (TextView)findViewById(R.id.current_temperature_field);
        humidity_field = (TextView)findViewById(R.id.humidity_field);
        pressure_field = (TextView)findViewById(R.id.pressure_field);
        weatherIcon = (TextView)findViewById(R.id.weather_icon);
        weatherIcon.setTypeface(weatherFont);


        Function.placeIdTask asyncTask =new Function.placeIdTask(new Function.AsyncResponse() {
            public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity, String weather_pressure, String weather_updatedOn, String weather_iconText, String sun_rise) {

                cityField.setText(weather_city);
                updatedField.setText(weather_updatedOn);
                detailsField.setText(weather_description);
                currentTemperatureField.setText(weather_temperature);
                humidity_field.setText("Humidity: "+weather_humidity);
                pressure_field.setText("Pressure: "+weather_pressure);
                weatherIcon.setText(Html.fromHtml(weather_iconText));

            }
        });



        asyncTask.execute(String.valueOf(lat), String.valueOf(lon));
    }
}
